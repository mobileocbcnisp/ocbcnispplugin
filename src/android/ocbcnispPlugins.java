package ocbcnisp.plugins.com;

//noverio debug begin: import LOG util to loging android logic here...
import android.content.Context;
import android.content.Intent;
import android.util.Log;
//noverio debug end

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * This class echoes a string called from JavaScript.
 */
public class ocbcnispPlugins extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("HelloWorldOCBCNISP")) {
            String message = args.getString(0);
            this.HelloWorldOCBCNISP(message, callbackContext);
            return true;
        }else if(action.equals("CallONeMobileAppBOX")){
            this.CallNewAppBoxIntent(args, callbackContext);
            return true;
        }
        return false;
    }

    private void HelloWorldOCBCNISP(String message, CallbackContext callbackContext) {
        callbackContext.success("Hello from ocbcnisp guys");
    }
    private void CallNewAppBoxIntent(JSONArray args,CallbackContext callbackContext){
        try {
            JSONObject ParamObj = new JSONObject(args.getString(0));
            String AppBoxPackageName = ParamObj.getJSONObject("Config").getString("PackageName");
            //Do Call AppBoxName Here: Example : AppBoxPackageName = "com.ocbcnisp.MBank"

            if(AppBoxPackageName.contentEquals("com.ocbcnisp.MBank")){
                callbackContext.success("Mbanking Called"+AppBoxPackageName);
                // Intent i = new Intent();
                // i.setClass(cordova.getActivity(), com.ocbcnisp.onemobile.Mbanking.StartActivity.class);
                // cordova.getActivity().startActivity(i);
            }
            else if(AppBoxPackageName.contentEquals("com.ocbcnisp.openAccount")){
                callbackContext.success("OpenAcc Called"+AppBoxPackageName);
            }
            else if(AppBoxPackageName.contentEquals("com.ocbcnisp.gocash")){
                callbackContext.success("MobileGoCash Called"+AppBoxPackageName);
            }
            else if(AppBoxPackageName.contentEquals("com.ocbcnisp.softToken")){
                callbackContext.success("softToken Called"+AppBoxPackageName);
            }
            else if(AppBoxPackageName.contentEquals("com.ocbcnisp.videoChat")){
                callbackContext.success("videoChat Called"+AppBoxPackageName);
            }
            else{
                callbackContext.success("PackageName Cannot Be Found");
            }
        } catch (JSONException JsonException) {
            JsonException.printStackTrace();
            callbackContext.error(JsonException.getMessage());
        }
        catch (Exception e){
            e.printStackTrace();
            callbackContext.error(e.getMessage());
        }
    }
}
