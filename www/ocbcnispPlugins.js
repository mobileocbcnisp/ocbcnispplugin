var exec = require('cordova/exec');

exports.HelloWorldOCBCNISP = function(arg0, success, error) {
    exec(success, error, "ocbcnispPlugins", "HelloWorldOCBCNISP", [arg0]);
};

exports.CallNewAppBoxIntent = function(arg0, success, error) {
    console.log("noverio debug begin on ocbcnispPlugins.js");
    console.log(arg0);
    exec(success, error, "ocbcnispPlugins", "CallONeMobileAppBOX", [arg0]);
};